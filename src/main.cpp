#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <string>

//Déclaration des fonctions
void Affichage_Erreur(ExecStatusType);
void Affichage_Tiret(unsigned int, int, std::string);
std::string Affichage_Donnee(char *, unsigned int);
void Recup_Affich_Donnee(PGconn *);

int main()
{
  char info_de_co[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.bouchet user=a.bouchet password=P@ssword";
  PGPing code_retour_ping;
  PGconn *connexion;
  int i, l;
  //int l = std::strlen (PQpass(_connexion));
  code_retour_ping = PQping(info_de_co);

  if(code_retour_ping == PQPING_OK)
  {
    // établissement de la connexion ou affichage des erreurs
    std::cout << "* Serveur accessible" << std::endl;
    connexion = PQconnectdb(info_de_co);
    l = std::strlen(PQpass(connexion));

    if(PQstatus(connexion) != CONNECTION_OK)
    {
      std::cerr << "* La connexion a échoué !" << std::endl;
      std::cerr << "* Le serveur n'est pas accessible pour le moment ! Veuillez contacter votre administrateur réseau" << std::endl;
    }
    else
    {
      std::cout << "* La connexion a été établie !" << std::endl;
      std::cout << "* La connexion au serveur de base de donnees : " << PQhost(connexion) << " a ete etablie avec les parametres suivants :\n" << std::endl;
      std::cout << "* Utilisateur : " << PQuser(connexion) << std::endl;
      std::cout << "* Mot de passe : ";

      //pour le mot de passe
      for(i = 0; i < l; ++i)
      {
        std::cout << "*";
      }

      std::cout << "\n* Base de donnees : " << PQdb(connexion) << std::endl;
      std::cout << "* Port TCP : " << PQport(connexion) << std::endl;

      if(PQsslInUse(connexion) == 1)
      {
        std::cout << "* Chiffrement SSL : true" << std::endl;
      }
      else
      {
        std::cout << "* Chiffrement SSL : false" << std::endl;
      }

      std::cout << "* Encodage : " << PQparameterStatus(connexion, "server_encoding") << std::endl;
      std::cout << "* Version du protocole : " << PQprotocolVersion(connexion) << std::endl;
      std::cout << "* Version du serveur : " << PQserverVersion(connexion) << std::endl;
      std::cout << "* Version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
      Recup_Affich_Donnee(connexion);
      return 0;
    }
  }
}

// Question 4
void Recup_Affich_Donnee(PGconn *connexion)
{
  unsigned int
    nbColonnes = 0,
    nbLignes = 0,
    longueur_max_champs = 0,
    longueur_titre = 0;
  std::string separateur = " | ";
  PGresult *requete = PQexec(connexion, "SET SCHEMA 'si6'; SELECT \"Animal\".id,\"Animal\".nom,sexe,date_naissance AS \"date de naissance\",commentaires,\"Race\".nom as race, description FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");
  
  if(PQresultStatus(requete) == PGRES_TUPLES_OK)
  {
    std::cout << "* Requete effectuee avec succès" << std::endl;
    // Question 8
    /*Affichage des noms de colonnes*/
    nbColonnes = PQnfields(requete);
    nbLignes = PQntuples(requete);

    for(unsigned int ligne = 0; ligne < nbLignes; ligne++)
    {
      for(unsigned int colonne = 0; colonne < nbColonnes; colonne++)
      {
        longueur_titre = std::strlen(PQfname(requete, colonne));

        if(longueur_titre > longueur_max_champs)
        {
          longueur_max_champs = longueur_titre;
        }
      }
    }

    // Question 12 & 13
    Affichage_Tiret(longueur_max_champs, nbColonnes, separateur);
    std::cout << separateur;

    for(unsigned int colonne = 0; colonne < nbColonnes; colonne++)
    {
      std::cout << std::left << std::setw(longueur_max_champs) << PQfname(requete, colonne) << separateur ;
    }
    std::cout << std::endl;

    Affichage_Tiret(longueur_max_champs, nbColonnes, separateur);

    // affichage des lignes normaliser
    for(unsigned int ligne = 0; ligne < nbLignes; ligne++)
    {
      std::cout << separateur;

      for(unsigned int colonne = 0; colonne < nbColonnes; colonne++)
      {
        std::cout << std::setw(longueur_max_champs) << Affichage_Donnee(PQgetvalue(requete, ligne, colonne), longueur_max_champs) << separateur;
      }

      std::cout << std::endl;
    }

    Affichage_Tiret(longueur_max_champs, nbColonnes, separateur);
  }
  else
  {
    std::cout << "* Erreur dans la requete !" << std::endl;
    Affichage_Erreur(PQresultStatus(requete));
  }

  PQclear(requete);
}


// Question 12
void Affichage_Tiret(unsigned int longueur_champs, int nombre_de_champs, std::string separateur)
{
  for(unsigned int num_caractere = 0; num_caractere < (nombre_de_champs * (longueur_champs + separateur.size()) + separateur.size()); ++num_caractere)
  {
    std::cout << "-";
  }
  std::cout << std::endl;
}

// Question 5
void Affichage_Erreur(ExecStatusType reussite)
{
  if(reussite == PGRES_FATAL_ERROR)
  {
    std::cerr << "* Erreur Fatale !! " << std::endl;
  }
  else if(reussite == PGRES_BAD_RESPONSE)
  {
    std::cerr << "* La réponse du serveur n'a pas été comprise ! " << std::endl;
  }
  else if(reussite == PGRES_EMPTY_QUERY)
  {
    std::cerr << "* Aucune requête ! " << std::endl;
  }
  else if(reussite == PGRES_NONFATAL_ERROR)
  {
    std::cerr << "* Erreur non Fatale " << std::endl;
  }
}
std::string Affichage_Donnee(char *info, unsigned int taille)
{
  std::string chaine;

  if(std::strlen(info) > taille)
  {
    for(unsigned int t = 0; t < taille - 3; t++)
    {
      chaine.push_back(info[t]);
    }

    chaine.append("...");
  }
  else
  {
    chaine.append(info);
  }

  return chaine;
}
